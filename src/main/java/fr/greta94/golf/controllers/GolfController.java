package fr.greta94.golf.controllers;

import fr.greta94.golf.models.CourseRepository;
import fr.greta94.golf.models.Golf;
import fr.greta94.golf.models.GolfRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GolfController {
    @Autowired
    private GolfRepository golfRepository;

    @Autowired
    private CourseRepository courseRepository;

    @RequestMapping("/golfs")
    public String golfs(Model model) {
        model.addAttribute("golfs", golfRepository.findAll());

        return "golfs";
    }

    @RequestMapping(value = "/golf/{id}", method = RequestMethod.GET)
    public String golf (Model model, @PathVariable("id") Long id) {
        Golf golf = golfRepository.findById(id).get();
        model.addAttribute("golf", golf);
        model.addAttribute("courses", courseRepository.findAllByGolf(golf));

        return "golf";
    }

    @RequestMapping(value = "/golf/{id}", method = RequestMethod.POST)
    public String modifyGolf(Model model, @PathVariable("id") Long id, @RequestParam("name") String name) {
        Golf golf = golfRepository.findById(id).get();
        golf.setName(name);
        golfRepository.save(golf);

        model.addAttribute("golf", golf);

        return "golf";
    }
}
