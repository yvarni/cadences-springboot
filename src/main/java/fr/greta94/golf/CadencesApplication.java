package fr.greta94.golf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class CadencesApplication {

    public static void main(String[] args) {
        SpringApplication.run(CadencesApplication.class, args);
    }

}
