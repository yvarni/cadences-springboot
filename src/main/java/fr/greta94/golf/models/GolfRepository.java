package fr.greta94.golf.models;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GolfRepository extends JpaRepository<Golf, Long> {
}
